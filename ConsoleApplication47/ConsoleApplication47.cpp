#include <iostream>
#include <ctime>
using namespace std;

typedef unsigned int uint;

uint degree(uint& a, uint& b);

int main()
{
	setlocale(LC_ALL, "ru");

	uint a;
	uint b;

	cout << "Возведение числа в степень" << endl << endl;
	cout << "Введите число: ";
	cin >> a;
	cout << endl;
	cout << "Введите степень: "; 
	cin >> b; 
	cout << endl;

	uint resultDegree = degree(a, b);

	cout << "Результат возведения в степень: " << resultDegree;

	cout << endl;
	return 0;
}

uint degree(uint& a, uint& b) {
	return pow(a, b);
}
